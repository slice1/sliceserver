﻿using System;
using System.Threading;
using System.Data.SqlClient;


namespace FrameworkServerAir
{
    class Program
    {
        private static Thread threadConsole;
        private static bool consoleIsRunning;

        static void Main(string[] args)
        {
            threadConsole = new Thread(new ThreadStart(ConsoleThread));
            threadConsole.Start();

            Globals.networkHandleData.InitMessages();
            Globals.general.InitServer();
            //Globals.gameLogic.ServerLoop();

            //ByteBuffer bart = new ByteBuffer();
            //bart.WriteBytes(bart.SetDeck(Globals.Accounts.retDeck()));
            //Console.WriteLine(bart.Length());
        }

        private static void ConsoleThread()
        {
            string line;
            consoleIsRunning = true;

            while (consoleIsRunning)
            {
                line = Console.ReadLine();

                if (String.IsNullOrEmpty(line))
                {
                    consoleIsRunning = false;
                    return;                    
                }
            }
        }
        
    }
}
