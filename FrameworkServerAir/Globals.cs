﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrameworkServerAir
{
    class Globals
    {
        

        public static Globals instance = new Globals();

        //Global instances of classes.
        public static General general = new General();
        public static Network network = new Network();
        public static NetworkHandleData networkHandleData = new NetworkHandleData();
        public static NetworkSendData networkSendData = new NetworkSendData();
        public static AccountContext Accounts = new AccountContext();
        public static GameContext Games = new GameContext();
        public static GameLogic gameLogic = new GameLogic();
        public static Random GetRandom = new Random();

        //
        public static Client[] Clients = new Client[Constants.MAX_PLAYERS];
        public static TempPlayer[] tempPlayers = new TempPlayer[Constants.MAX_PLAYERS];
        public static Player[] Player = new Player[Constants.MAX_PLAYERS];
        public static Lobby[] Lobbies = new Lobby[Constants.MAX_LOBBIES];
       
        //
        public int Player_HighIndex;
    }
}
