﻿using FrameworkServerAir.GameEntity;
using System;
using System.Collections.Generic;
using System.Text;

namespace FrameworkServerAir
{
    public class Lobby
    {
        public int Index;
        public int Diffiult;
        public string Name;
        public string Password;

        public bool IsInGame;
        public int curPlayer = 0;

        public List<Player> Players { get; set; }
        public List<List<Character>> CharMap;
        public Game game { get; set; }
        


        public Lobby()
        {
            Players = new List<Player>();
            
        }














        public void newLobby(Player player)
        {
            game = new Game { Id = Globals.GetRandom.Next(), IsPlaying = false };
            

            IsInGame = false;
            player.game = game;
            player.lobbyIndex = Index;

            Players.Add(player);
        }

        public void AddPlayer(Player player)
        {
            Players.Add(player);
            player.lobbyIndex = Index;
            player.inPlay = true;
            player.inSearch = false;
        }

        public void KickPlayer(Player player)
        {
            Players.Remove(player);
            player.lobbyIndex = 0;
            player.inPlay = false;
        }


        public void setWinner(Player player)
        {
            player.Winner = true;
            player.game.IsPlaying = false;
        }

        public void StartGame()
        {
            IsInGame = true;

        }

        public void CloseLobby()
        {
            Console.WriteLine("Lobby: " + Name + " start closing.");
            Globals.Lobbies[Index] = null;
            Console.WriteLine("Lobby: " + Name + " closed.");
        }









        public void EndTurn()
        {
            curPlayer++;
            if (curPlayer == Players.Count)
                curPlayer = 0;
        }



































        public void MoveBlock(int x, int y, int dir, int dist)
        {
            List<Character> moved = new List<Character>();

            ArrToMove(x, y, dir, moved);


            switch (dir)
            {
                case 1:
                    foreach (Character c in moved)
                    {
                        c.closeX = c._x + dist;
                    }
                    break;

                case 2:
                    foreach (Character c in moved)
                    {
                        c.closeY = c._y + dist;
                    }
                    break;
                case 3:
                    foreach (Character c in moved)
                    {
                        c.closeX = c._x - dist;
                    }
                    break;
                case 4:
                    foreach (Character c in moved)
                    {
                        c.closeY = c._y - dist;
                    }
                    break;
                default:
                    break;
            }

            foreach (Character c in moved)
            {

            }

            foreach (Character c in moved)
            {

            }


        }














        void ArrToMove(int x, int y, int dir, List<Character> Arr)
        {
            if (dir == 1 || dir == 3)
            {
                for (int i = 0; i < Diffiult; i++)
                {
                    if (x + i < Diffiult)
                    {
                        Arr.Add(CharMap[x + i][y]);
                    }
                    if (x - i >= 0 && i != 0)
                    {
                        Arr.Add(CharMap[x - i][y]);
                    }
                }
            }
            else
            {
                for (int i = 0; i < Diffiult; i++)
                {
                    if (y + i < Diffiult)
                    {
                        Arr.Add(CharMap[x][y + i]);
                    }
                    if (y - i >= 0 && i != 0)
                    {
                        Arr.Add(CharMap[x][y - i]);
                    }
                }
            }
        }
    }
}
