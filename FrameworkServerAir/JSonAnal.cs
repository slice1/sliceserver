﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FrameworkServerAir
{
    public class ByteBuffer
    {
        #region "Getter"
        public Account GetShip(byte[] bytes)
        {
            string someString = Encoding.ASCII.GetString(bytes);
            Account ship = (Account)JsonConvert.DeserializeObject(someString);
            return ship;
        }
        //public Card GetCard(byte[] bytes)
        //{
        //    string someString = Encoding.ASCII.GetString(bytes);
        //    Card ship = (Card)JsonConvert.DeserializeObject(someString);
        //    return ship;
        //}
        public Game GetGame(byte[] bytes)
        {
            string someString = Encoding.ASCII.GetString(bytes);
            Game ship = (Game)JsonConvert.DeserializeObject(someString);
            return ship;
        }
        //public Deck GetDeck(byte[] bytes)
        //{
        //    string someString = Encoding.ASCII.GetString(bytes);
        //    Deck ship = (Deck)JsonConvert.DeserializeObject(someString);
        //    return ship;
        //}
        public Player GetPlayer(byte[] bytes)
        {
            string someString = Encoding.ASCII.GetString(bytes);
            Player ship = (Player)JsonConvert.DeserializeObject(someString);
            return ship;
        }
        #endregion Getter
        #region "Serrer"
        public byte[] SetPlayer(Player json)
        {
            var sara = JsonConvert.SerializeObject(json);
            byte[] bytes = Encoding.ASCII.GetBytes(sara);
            return bytes;
        }
        //public byte[] SetCard(Card json)
        //{
        //    var sara = JsonConvert.SerializeObject(json);
        //    byte[] bytes = Encoding.ASCII.GetBytes(sara);
        //    return bytes;
        //}
        public byte[] SetGame(Game json)
        {
            var sara = JsonConvert.SerializeObject(json);
            byte[] bytes = Encoding.ASCII.GetBytes(sara);
            return bytes;
        }
        //public byte[] SetDeck(Deck json)
        //{
        //    var sara = JsonConvert.SerializeObject(json);
        //    byte[] bytes = Encoding.ASCII.GetBytes(sara);
        //    return bytes;
        //}
        public byte[] SetAccount(Account json)
        {
            var sara = JsonConvert.SerializeObject(json);
            byte[] bytes = Encoding.ASCII.GetBytes(sara); 
            return bytes;
        }
        #endregion "Serrer"



        List<byte> Buff;
        byte[] readBuff;
        int readpos;
        bool buffUpdate = false;

        public ByteBuffer()
        {
            Buff = new List<byte>();
            readpos = 0;
        }

        public int GetReadPos()
        {
            return readpos;
        }

        public byte[] ToArray()
        {
            return Buff.ToArray();
        }

        public int Count()
        {
            return Buff.Count;
        }

        public int Length()
        {
            return Count() - readpos;
        }

        public void Clear()
        {
            Buff.Clear();
            readpos = 0;
        }
        #region"Write Data"
        public void WriteByte(byte Inputs)
        {
            Buff.Add(Inputs);
            buffUpdate = true;
        }

        public void WriteBytes(byte[] Input)
        {
            Buff.AddRange(Input);
            buffUpdate = true;
        }

        public void WriteShort(short Input)
        {
            Buff.AddRange(BitConverter.GetBytes(Input));
            buffUpdate = true;
        }

        public void WriteInteger(int Input)
        {
            Buff.AddRange(BitConverter.GetBytes(Input));
            buffUpdate = true;
        }

        public void WriteFloat(float Input)
        {
            Buff.AddRange(BitConverter.GetBytes(Input));
            buffUpdate = true;
        }



        public void WriteString(string Input)
        {
            Buff.AddRange(BitConverter.GetBytes(Input.Length));
            Buff.AddRange(Encoding.ASCII.GetBytes(Input));
            buffUpdate = true;
        }
        #endregion

        #region "Read Data"
        public string ReadString(bool Peek = true)
        {
            int len = ReadInteger(true);
            if (buffUpdate)
            {
                readBuff = Buff.ToArray();
                buffUpdate = false;
            }

            string ret = Encoding.ASCII.GetString(readBuff, readpos, len);
            if (Peek & Buff.Count() > readpos)
            {
                if (ret.Length > 0)
                {
                    readpos += len;
                }
            }
            return ret;
        }

        public byte ReadByte(bool Peek = true)
        {
            if (Buff.Count() > readpos)
            {
                if (buffUpdate)
                {
                    readBuff = Buff.ToArray();
                    buffUpdate = false;
                }

                byte ret = readBuff[readpos];
                if (Peek & Buff.Count() > readpos)
                {
                    readpos += 1;
                }
                return ret;
            }

            else
            {
                throw new Exception("Byte Buffer Past Limit!");
            }
        }

        public byte[] ReadBytes(int Length, bool Peek = true)
        {
            if (buffUpdate)
            {
                readBuff = Buff.ToArray();
                buffUpdate = false;
            }

            byte[] ret = Buff.GetRange(readpos, Length).ToArray();
            if (Peek)
            {
                readpos += Length;
            }
            return ret;
        }

        public float ReadFloat(bool Peek = true)
        {
            if (Buff.Count() > readpos)
            {
                if (buffUpdate)
                {
                    readBuff = Buff.ToArray();
                    buffUpdate = false;
                }

                float ret = BitConverter.ToSingle(readBuff, readpos);
                if (Peek & Buff.Count() > readpos)
                {
                    readpos += 4;
                }
                return ret;
            }

            else
            {
                throw new Exception("Byte Buffer is Past its Limit!");
            }
        }

        public int ReadInteger(bool Peek = true)
        {
            if (Buff.Count() > readpos)
            {
                if (buffUpdate)
                {
                    readBuff = Buff.ToArray();
                    buffUpdate = false;
                }

                int ret = BitConverter.ToInt32(readBuff, readpos);
                if (Peek & Buff.Count() > readpos)
                {
                    readpos += 4;
                }
                return ret;
            }

            else
            {
                throw new Exception("Byte Buffer is Past its Limit!");
            }
        }
        #endregion

        private bool disposedValue = false;

        //IDisposable
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposedValue)
            {
                if (disposing)
                {
                    Buff.Clear();
                }

                readpos = 0;
            }
            this.disposedValue = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
