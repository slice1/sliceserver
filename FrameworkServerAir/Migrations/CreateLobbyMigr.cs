﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.Migrations;

namespace FrameworkServerAir.Migrations
{
    class CreateLobbyMigr : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                    "dbo.Lobby",
                    c => new
                    {
                        LobbyId = c.Int(nullable: false, identity: true),
                        Title = c.String(maxLength: 200),
                        Content = c.String(),
                        GameId = c.Int(nullable: false),
                    })
                    .PrimaryKey(t => t.LobbyId)
                    .ForeignKey("dbo.Games", t => t.GameId, cascadeDelete: true)
                    .Index(t => t.GameId)
                    .Index(p => p.Title, unique: true);

            AddColumn("dbo.Blogs", "Rating", c => c.Int(nullable: false, defaultValue: 3));

            //AddForeignKey("dbo.Games", t => t.LobbyId, )
        }
    
    }
}
