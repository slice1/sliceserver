namespace FrameworkServerAir.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class testMig : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Cards", "Deck_Id", "dbo.Decks");
            DropForeignKey("dbo.AccountsTables", "_deck_Id", "dbo.Decks");
            DropIndex("dbo.AccountsTables", new[] { "_deck_Id" });
            DropIndex("dbo.Cards", new[] { "Deck_Id" });
            DropTable("dbo.Players");
            CreateTable(
                "dbo.Games",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsPlaying = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Players",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        _inf = c.Int(nullable: false),
                        _hp = c.Int(nullable: false),
                        _time = c.Single(nullable: false),
                        Winner = c.Boolean(nullable: false),
                        Gold = c.Int(nullable: false),
                        CardsInHand = c.Int(nullable: false),
                        enemy_Id = c.Int(),
                        game_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AccountsTables", t => t.Id)
                .ForeignKey("dbo.Players", t => t.enemy_Id)
                .ForeignKey("dbo.Games", t => t.game_Id)
                .Index(t => t.Id)
                .Index(t => t.enemy_Id)
                .Index(t => t.game_Id);
            
            AddColumn("dbo.AccountsTables", "_gold", c => c.Int(nullable: false));
            AddColumn("dbo.AccountsTables", "_game_Id", c => c.Int());
            CreateIndex("dbo.AccountsTables", "_game_Id");
            AddForeignKey("dbo.AccountsTables", "_game_Id", "dbo.Games", "Id");
            DropColumn("dbo.AccountsTables", "_deck_Id");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Cards",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        _hp = c.Int(nullable: false),
                        Deck_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Decks",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.AccountsTables", "_deck_Id", c => c.Int());
            DropForeignKey("dbo.AccountsTables", "_game_Id", "dbo.Games");
            DropForeignKey("dbo.Players", "game_Id", "dbo.Games");
            DropForeignKey("dbo.Players", "enemy_Id", "dbo.Players");
            DropForeignKey("dbo.Players", "Id", "dbo.AccountsTables");
            DropIndex("dbo.Players", new[] { "game_Id" });
            DropIndex("dbo.Players", new[] { "enemy_Id" });
            DropIndex("dbo.Players", new[] { "Id" });
            DropIndex("dbo.AccountsTables", new[] { "_game_Id" });
            DropColumn("dbo.AccountsTables", "_game_Id");
            DropColumn("dbo.AccountsTables", "_gold");
            DropTable("dbo.Players");
            DropTable("dbo.Games");
            CreateIndex("dbo.Cards", "Deck_Id");
            CreateIndex("dbo.AccountsTables", "_deck_Id");
            AddForeignKey("dbo.AccountsTables", "_deck_Id", "dbo.Decks", "Id");
            AddForeignKey("dbo.Cards", "Deck_Id", "dbo.Decks", "Id");
        }
    }
}
