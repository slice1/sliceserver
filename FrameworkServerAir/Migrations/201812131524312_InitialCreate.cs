namespace FrameworkServerAir.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AccountsTables",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Password = c.String(),
                        _deck_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Decks", t => t._deck_Id)
                .Index(t => t._deck_Id);
            
            CreateTable(
                "dbo.Decks",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Cards",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        _hp = c.Int(nullable: false),
                        Deck_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Decks", t => t.Deck_Id)
                .Index(t => t.Deck_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AccountsTables", "_deck_Id", "dbo.Decks");
            DropForeignKey("dbo.Cards", "Deck_Id", "dbo.Decks");
            DropIndex("dbo.Cards", new[] { "Deck_Id" });
            DropIndex("dbo.AccountsTables", new[] { "_deck_Id" });
            DropTable("dbo.Cards");
            DropTable("dbo.Decks");
            DropTable("dbo.AccountsTables");
        }
    }
}
