﻿using FrameworkServerAir.GameEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrameworkServerAir
{
    class NetworkHandleData
    {
        private delegate void Packet_(int Index, byte[] Data);
        private Dictionary<int, Packet_> Packets;



        public void InitMessages()
        {
            Packets = new Dictionary<int, Packet_>();
            Packets.Add((int)Enumerations.ClientPackets.CNewAccount, HandleNewAccount);
            Packets.Add((int)Enumerations.ClientPackets.CLoginAndSearchLobby, HandleLoginAndSearchLobby);
            Packets.Add((int)Enumerations.ClientPackets.CLoginAndCreateLobby, HandleLoginAndCreateLobby);
            Packets.Add((int)Enumerations.ClientPackets.CStartGame, HandleStartLobby);
            Packets.Add((int)Enumerations.ClientPackets.CMoveBlock, HandleMoveBlock);
            Packets.Add((int)Enumerations.ClientPackets.CEndTurn, HandleEndTurn);

            //Packets.Add((int)Enumerations.ClientPackets.CHandleDeck, HandleDeckChangePos);
            //Packets.Add((int)Enumerations.ClientPackets.CSearchLobby, HadleLookForLobby);
            //Packets.Add((int)Enumerations.ClientPackets.CCreateLobby, HandleCreateLobby);
        }



        public void HandleData(int index, byte[] data)
        {
            int packetnum;
            Packet_ Packet;
            ByteBuffer buffer = new ByteBuffer();
            buffer.WriteBytes(data);
            packetnum = buffer.ReadInteger();
            buffer = null;

            if (packetnum == 0)
                return;

            if (Packets.TryGetValue(packetnum, out Packet))
            {
                Packet.Invoke(index, data);
            }

        }

        void HandleNewAccount(int index, byte[] data)
        {
            ByteBuffer buffer = new ByteBuffer();
            buffer.WriteBytes(data);
            int packetNum = buffer.ReadInteger();
            string username = buffer.ReadString();
            string password = buffer.ReadString();

            Globals.Accounts.addAcc(username, password);
        }

        void HandleLoginAndSearchLobby(int index, byte[] data)
        {
            ByteBuffer buffer = new ByteBuffer();
            buffer.WriteBytes(data);
            int packetNum = buffer.ReadInteger();
            string username = buffer.ReadString();
            string password = buffer.ReadString();
            string lobbyName = buffer.ReadString();

            int SearchedLobbyIndex = 0;

            Console.WriteLine("Incoming searching for lobby. Lobbyname: " + lobbyName);
            foreach (Lobby l in Globals.Lobbies)
            {
                if (l != null)
                {
                    if (l.Name == lobbyName)
                    {
                        SearchedLobbyIndex = l.Index;
                        break;
                    }
                }
            }
            if (SearchedLobbyIndex == 0)
            {
                Globals.networkSendData.SendAlertMsg(index, "Lobby didn't find.");
                return;
            }



            Console.WriteLine("Incoming loging. Username: " + username +" || Password: " + password);
            if (!Globals.Accounts.AccountExist(index, username))
            {
                
                Globals.networkSendData.SendAlertMsg(index, "Username does not exist. Try again.");
                return;
            }

            if (!Globals.Accounts.PasswordOK(index, username, password))
            {
                Globals.networkSendData.SendAlertMsg(index, "Password incorrect. Try again.");
                return;
            }

            Console.WriteLine("Player " + username + " logged in succesfully.");
            Globals.Accounts.LoadPlayer(index, username);


            Globals.Player[index].lobbyIndex = SearchedLobbyIndex;

            Globals.Lobbies[SearchedLobbyIndex].AddPlayer(Globals.Player[index]);

            Globals.networkSendData.SendLobbyFound(index);
            Globals.networkSendData.SendLobbyAddedPlayer(SearchedLobbyIndex, index);
        }


        void HandleLoginAndCreateLobby(int index, byte[] data)
        {
            ByteBuffer buffer = new ByteBuffer();
            buffer.WriteBytes(data);
            int packetNum = buffer.ReadInteger();

            string username = buffer.ReadString();
            string password = buffer.ReadString();
            string lobbyName = buffer.ReadString();

            Console.WriteLine("Incoming loging. Username: " + username + " || Password: " + password);
            if (!Globals.Accounts.AccountExist(index, username))
            {

                Globals.networkSendData.SendAlertMsg(index, "Username does not exist. Try again.");
                return;
            }

            if (!Globals.Accounts.PasswordOK(index, username, password))
            {
                Globals.networkSendData.SendAlertMsg(index, "Password incorrect. Try again.");
                return;
            }

            Console.WriteLine("Player " + username + " logged in succesfully.");
            Globals.Accounts.LoadPlayer(index, username);



            Console.WriteLine("Incoming creating lobby. Lobbyname: " + lobbyName);
            Lobby lobby = new Lobby();
            lobby.newLobby(Globals.Player[index]);
            lobby.Name = lobbyName;
            lobby.Index = index;

            Globals.Lobbies[index] = lobby;
            Globals.Player[index].lobbyIndex = index;
            Globals.Player[index].inPlay = true;
            Globals.Player[index].inSearch = false;
            Console.WriteLine("Lobby Created. Lobbyname: " + lobbyName);

            Globals.tempPlayers[index].inLobby = true;

            Globals.networkSendData.SendLobbyCreated(index);
        }


        void HandleStartLobby(int index, byte[] data)
        {


            ByteBuffer buffer = new ByteBuffer();
            buffer.WriteBytes(data);
            int packetNum = buffer.ReadInteger();
            int Difficulty = buffer.ReadInteger();

            Console.WriteLine("Incoming starting lobby from: " + Globals.Player[index].Nickname + " || with difficulty: " + Difficulty);

            if (Globals.Lobbies[Globals.Player[index].lobbyIndex].Players[0].index != index)
            {
                Globals.networkSendData.SendAlertMsg(index, "You aren't creator of the game!");
                Console.WriteLine("Incoming starting lobby from " + Globals.Player[index].Nickname + " isn't done. Player isn't creator.");
                return;
            }



            Globals.networkSendData.SendLobbyStarted(Globals.Player[index].lobbyIndex, Difficulty);
            Globals.networkSendData.SendGamePlayerStartTurn(Globals.Lobbies[Globals.Player[index].lobbyIndex].Players[Globals.Lobbies[Globals.Player[index].lobbyIndex].curPlayer].index);
        }




        ///Game Logic//////Game Logic//////Game Logic//////Game Logic//////Game Logic//////Game Logic//////Game Logic//////Game Logic//////Game Logic//////Game Logic//////Game Logic//////Game Logic//////Game Logic///
        #region GameLogic

        void HandleMoveBlock(int index, byte[] data)
        {


            ByteBuffer buffer = new ByteBuffer();
            buffer.WriteBytes(data);
            int packetNum = buffer.ReadInteger();
            int _X = buffer.ReadInteger();
            int _Y = buffer.ReadInteger();
            int Dir = buffer.ReadInteger();
            int Distance = buffer.ReadInteger();


            if (Globals.Lobbies[Globals.Player[index].lobbyIndex] == null)
            {
                Globals.networkSendData.SendAlertMsg(index, "Lobby doesn't exist!");
                Console.WriteLine("Incoming FAILED move from " + Globals.Player[index].Nickname + ". Lobby doesn't exist");
                return;
            }

            Console.WriteLine("Incoming move from: " + Globals.Player[index].Nickname + " || Move block: " + _X + '/' + _Y + " at dir: " + Dir + " on " + Distance);
            

            Globals.networkSendData.SendGamePlayerMoveBlock(index, _X, _Y, Dir, Distance);
        }

        void HandleDoAct(int index, byte[] data)
        {


            ByteBuffer buffer = new ByteBuffer();
            buffer.WriteBytes(data);
            int packetNum = buffer.ReadInteger();
            int Dir = buffer.ReadInteger();
            int Distance = buffer.ReadInteger();
            int Difficulty = 0;

            Console.WriteLine("Incoming starting lobby from: " + Globals.Player[index].Nickname + " || with difficulty: " + Difficulty);

            if (Globals.Lobbies[Globals.Player[index].lobbyIndex].Players[0].index != index)
            {
                Globals.networkSendData.SendAlertMsg(index, "You aren't creator of the game!");
                Console.WriteLine("Incoming starting lobby from " + Globals.Player[index].Nickname + " isn't done. Player isn't creator.");
                return;
            }



            Globals.networkSendData.SendLobbyStarted(Globals.Player[index].lobbyIndex, Difficulty);
        }

        void HandleEndTurn(int index, byte[] data)
        {


            ByteBuffer buffer = new ByteBuffer();
            buffer.WriteBytes(data);
            int packetNum = buffer.ReadInteger();

            Console.WriteLine("Incoming ending Turn from: " + Globals.Player[index].Nickname);

            if (Globals.Lobbies[Globals.Player[index].lobbyIndex].Players[Globals.Lobbies[Globals.Player[index].lobbyIndex].curPlayer].index != index)
            {
                Globals.networkSendData.SendAlertMsg(index, "That isn't your turn.");
                Console.WriteLine("Incoming ending Turn from " + Globals.Player[index].Nickname + " isn't done. It isn't his turn.");
                return;
            }


            Console.WriteLine("Player " + Globals.Lobbies[Globals.Player[index].lobbyIndex].Players[Globals.Lobbies[Globals.Player[index].lobbyIndex].curPlayer].Nickname + " ends his turn.");
            Globals.networkSendData.SendGamePlayerEndTurn(Globals.Lobbies[Globals.Player[index].lobbyIndex].Players[Globals.Lobbies[Globals.Player[index].lobbyIndex].curPlayer].index);
            Globals.Lobbies[Globals.Player[index].lobbyIndex].EndTurn();


            Console.WriteLine("Player " + Globals.Lobbies[Globals.Player[index].lobbyIndex].Players[Globals.Lobbies[Globals.Player[index].lobbyIndex].curPlayer].Nickname + " starts his turn.");
            Globals.networkSendData.SendGamePlayerStartTurn(Globals.Lobbies[Globals.Player[index].lobbyIndex].Players[Globals.Lobbies[Globals.Player[index].lobbyIndex].curPlayer].index);
        }


        #endregion GameLogic
        ///End Game Logic//////End Game Logic//////End Game Logic//////End Game Logic//////End Game Logic//////End Game Logic//////End Game Logic//////End Game Logic//////End Game Logic//////End Game Logic///


        //void HadleLookForLobby(int index, byte[] data)
        //{
        //    ByteBuffer buffer = new ByteBuffer();
        //    buffer.WriteBytes(data);
        //    int packetNum = buffer.ReadInteger();


        //    Globals.Player[index].inSearch = true;
        //}


        void HandleDeckChangePos(int index, byte[] data)
        {
            //ByteBuffer buffer = new ByteBuffer();
            //buffer.WriteBytes(data);
            //int packetNum = buffer.ReadInteger();
            //byte[] deckB = buffer.ReadBytes(4);
            //Deck deck = buffer.GetDeck(deckB);
            //byte[] deckDeckB = buffer.ReadBytes(4);
            //Deck GDeckDeck = buffer.GetDeck(deckDeckB);
            //byte[] deckHandB = buffer.ReadBytes(4);
            //Deck GDeckHand = buffer.GetDeck(deckHandB);
            //byte[] deckBFB = buffer.ReadBytes(4);
            //Deck GDeckBF = buffer.GetDeck(deckBFB);
            //byte[] deckGraveB = buffer.ReadBytes(4);
            //Deck GDeckGrave = buffer.GetDeck(deckGraveB);
            //byte[] deckStackB = buffer.ReadBytes(4);
            //Deck GDeckStack = buffer.GetDeck(deckStackB);
            //byte[] deckEnemyDeckBattlefieldB = buffer.ReadBytes(4);
            //Deck GEnemyDeckBattlefield = buffer.GetDeck(deckEnemyDeckBattlefieldB);
            //byte[] deckEnemyDeckStackB = buffer.ReadBytes(4);
            //Deck GEnemyDeckStack = buffer.GetDeck(deckEnemyDeckStackB);

            //float targetTime = buffer.ReadFloat();
            //int _hp = buffer.ReadInteger();
            //int _inf = buffer.ReadInteger();

            //Globals.general.SetPlayerDeck(index, deck);
            //Globals.general.SetPlayerGDeckBF(index, GDeckBF);
            //Globals.general.SetPlayerGDeckDeck(index, GDeckDeck);
            //Globals.general.SetPlayerGDeckGrave(index, GDeckGrave);
            //Globals.general.SetPlayerGDeckHand(index, GDeckHand);
            //Globals.general.SetPlayerGDeckStack(index, GDeckStack);
            //Globals.general.SetEnemyGDeckBF(index, GEnemyDeckBattlefield);
            //Globals.general.SetEnemyGDeckStack(index, GEnemyDeckStack);
            //Globals.general.SetPlayerHP(index, _hp);
            //Globals.general.SetPlayerInf(index, _inf);
            //Globals.general.SetPlayerTime(index, targetTime);

            
            //Globals.networkSendData.SendPlayerDecks(index, deck, GDeckDeck, GDeckHand, GDeckBF, GDeckGrave, GDeckStack, GEnemyDeckStack, GEnemyDeckBattlefield, GDeckHand.cards.Count, targetTime, _hp, _inf);
        }

    }
}
