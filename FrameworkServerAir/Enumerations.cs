﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrameworkServerAir
{
    class Enumerations
    {
        public enum ServerPackets
        {
            SAlertMsg = 1,
            SPlayerData,
            SLobbyUpdate,
            SLobbyCreated,
            SLobbyFound,
            SLobbyAddPlayer,
            SLobbyStart,
            SGameStartTurn,
            SGameMoveBlock,
            SGameEndTurn
        }

        public enum ClientPackets
        {
            CNewAccount = 1,
            CLoginAndCreateLobby,
            CLoginAndSearchLobby,
            CStartGame,
            CMoveBlock,
            CDoAct,
            CEndTurn
        }
    }
}


