﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrameworkServerAir.GameEntity
{
    public class Character
    {
        public int _FirstName, _SecondName, _power, _wisdom, _loyalty, _sprite, _INTPoliceLoyal, _INTMafiaLoyal, _INTAssasinLoyal, _INTCultLoyal;
        public int _isMale;
        public int _roleID;
        public int _itemID;

        public int closeX, closeY, _x, _y;

        public Character(int FirstName, int SecondName, int isMale, int power, int wisdom, int loyalty, int sprite, int INTPoliceLoyal, int INTMafiaLoyal, int INTAssasinLoyal, int INTCultLoyal, int roleID, int itemID)
        {
            _FirstName = FirstName;
            _SecondName = SecondName;
            _power = power;
            _wisdom = wisdom;
            _loyalty = loyalty;
            _sprite = sprite;
            _INTPoliceLoyal = INTPoliceLoyal;
            _INTMafiaLoyal = INTMafiaLoyal;
            _INTAssasinLoyal = INTAssasinLoyal;
            _INTCultLoyal = INTCultLoyal;
            _isMale = isMale;
            _roleID = roleID;
            _itemID = itemID;
        }
    }


    public class CharacterBuilder
    {
        int MaxFNames, MaxMnames, MaxFPorts, MaxMPorts, maxPow, minPow, maxWis, mixWis, maxLoy, minLoy;
        

        public CharacterBuilder(int maxFNames, int maxMnames, int maxFPorts, int maxMPorts, int maxPow, int minPow, int maxWis, int mixWis, int maxLoy, int minLoy)
        {
            MaxFNames = maxFNames;
            MaxMnames = maxMnames;
            MaxFPorts = maxFPorts;
            MaxMPorts = maxMPorts;
            this.maxPow = maxPow;
            this.minPow = minPow;
            this.maxWis = maxWis;
            this.mixWis = mixWis;
            this.maxLoy = maxLoy;
            this.minLoy = minLoy;
        }


        public Character GenCharacter()
        {
            int INTPoliceLoyal, INTMafiaLoyal, INTAssasinLoyal, INTCultLoyal;

            INTPoliceLoyal = Globals.GetRandom.Next(0, 4);
            INTMafiaLoyal = Globals.GetRandom.Next(0, 4);
            INTAssasinLoyal = Globals.GetRandom.Next(0, 4);
            INTCultLoyal = Globals.GetRandom.Next(0, 5);

            Character _character = GenCharacter(INTPoliceLoyal, INTMafiaLoyal, INTAssasinLoyal, INTCultLoyal);

            return _character;
        }

        public Character GenCharacter(int INTPoliceLoyal, int INTMafiaLoyal, int INTAssasinLoyal, int INTCultLoyal)
        {
            int _power = Globals.GetRandom.Next(minPow, maxPow);
            int _wisdom = Globals.GetRandom.Next(mixWis, maxWis);
            int _loyalty = Globals.GetRandom.Next(minLoy, maxLoy);
            int _ismale;

            int _sprite;
            int _firstName;

            if (100 - Globals.GetRandom.Next(0, 100) > 35)
            {
                _ismale = 1;

                _sprite = Globals.GetRandom.Next(0, MaxMPorts);
                _firstName = Globals.GetRandom.Next(0, MaxMnames);
            }
            else
            {
                _ismale = 0;

                _sprite = Globals.GetRandom.Next(0, MaxFPorts);
                _firstName = Globals.GetRandom.Next(0, MaxFNames);
            }

            Character _character = GenCharacter(_firstName, 0, _ismale, _power, _wisdom, _loyalty, _sprite, 2, 1, 0, 0, 0, 0);

            return _character;
        }


        public Character GenCharacter(int FirstName, int SecondName, int isMale, int power, int wisdom, int loyalty, int sprite, int INTPoliceLoyal, int INTMafiaLoyal, int INTAssasinLoyal, int INTCultLoyal, int roleID, int itemID)
        {
            Character _character = new Character(FirstName, SecondName, isMale, power, wisdom, loyalty, sprite, INTPoliceLoyal, INTMafiaLoyal, INTAssasinLoyal, INTCultLoyal, roleID, itemID);

            return _character;
        }

    }
}
