﻿using FrameworkServerAir.GameEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrameworkServerAir
{
    class NetworkSendData
    {
        public void SendDataTo(int index, byte[] data)
        {
            ByteBuffer buffer = new ByteBuffer();
            buffer.WriteBytes(data);
            Globals.Clients[index].myStream.BeginWrite(buffer.ToArray(), 0, buffer.ToArray().Length, null, null);
            buffer = null;
        }

        public async void SendDataToAll(byte[] data)
        {
            for (int i = 1; i < Constants.MAX_PLAYERS; i++)
            {
                if (Globals.tempPlayers[i].inLobby == true)
                {
                    await Task.Delay(75);
                    SendDataTo(i, data);
                }
            }
        }

        public async void SendDataToAllBut(int index, byte[] data)
        {
            for (int i = 1; i < Constants.MAX_PLAYERS; i++)
            {
                if (Globals.Clients[i].Socket != null)
                {
                    if (i != index)
                    {
                        await Task.Delay(75);
                        SendDataTo(i, data);
                    }
                }
            }
        }

        public async void SendDataToAllInLobby(int lobbyIndex, byte[] data)
        {
            for (int i = 0; i < Globals.Lobbies[lobbyIndex].Players.Count; i++)
            {
                if (Globals.Clients[Globals.Lobbies[lobbyIndex].Players[i].index].Socket != null)
                {
                    await Task.Delay(75);

                    SendDataTo(Globals.Lobbies[lobbyIndex].Players[i].index, data);
                    
                }
            }
        }


        public async void SendDataToAllInLobbyBut(int lobbyIndex, int PlayerIndex, byte[] data)
        {
            for (int i = 0; i < Globals.Lobbies[lobbyIndex].Players.Count; i++)
            {
                if (Globals.Clients[Globals.Lobbies[lobbyIndex].Players[i].index].Socket != null)
                {
                    await Task.Delay(75);
                    if (Globals.Lobbies[lobbyIndex].Players[i].index != PlayerIndex)
                        SendDataTo(Globals.Lobbies[lobbyIndex].Players[i].index, data);
                }
            }
        }



        public byte[] PlayerData(int index)
        {
            ByteBuffer buffer = new ByteBuffer();
            //prevent out of bounce
            if (index > Constants.MAX_PLAYERS)
                return null;

            buffer.WriteInteger((int)Enumerations.ServerPackets.SPlayerData);
            buffer.WriteInteger(index);
            buffer.WriteFloat(Globals.general.GetPlayerTime(index));
            buffer.WriteInteger(Globals.general.GetPlayerInf(index));
            buffer.WriteInteger(Globals.general.GetPlayerHp(index));
            buffer.WriteString(Globals.Player[index].Nickname);

            return buffer.ToArray();
        }

        public void SendAlertMsg(int index, string alertMsg)
        {
            ByteBuffer buffer = new ByteBuffer();

            buffer.WriteInteger((int)Enumerations.ServerPackets.SAlertMsg);
            buffer.WriteString(alertMsg);

            SendDataTo(index, buffer.ToArray());
        }



        //public void SendInlogin(int index)
        //{
        //    ByteBuffer buffer = new ByteBuffer();
        //    buffer.WriteInteger((int)Enumerations.ServerPackets.SLogin);
        //    buffer.WriteString(Globals.Player[index].Nickname);
        //    SendDataTo(index, buffer.ToArray());

        //    //Swnd Player data to everyone incl himself
        //    //SendDataTo(index, PlayerData(index));
        //    //Send all players to the player himself
        //    //for (int i = 1; i < Constants.MAX_PLAYERS; i++)
        //    //{
        //    //    if (Globals.general.IsPlaying(i))
        //    //    {
        //    //        if (i != index)
        //    //        {
        //    //            await Task.Delay(75);
        //    //            SendDataTo(index, PlayerData(i));
        //    //        }
        //    //    }
        //    //}

        //    Console.WriteLine("Player: " + Globals.Player[index].Nickname + " login sent.");

        //}


        public void SendLobbyData(int index)
        {
            int countOfPlayers = Globals.Lobbies[Globals.Player[index].index].Players.Count;
            ByteBuffer buffer = new ByteBuffer();
            buffer.WriteInteger((int)Enumerations.ServerPackets.SLobbyUpdate);
            buffer.WriteInteger(countOfPlayers);

            for (int i = 0; i < countOfPlayers; i++)
            {
                buffer.WriteString(Globals.Lobbies[Globals.Player[index].index].Players[i].Nickname);
            }

            SendDataTo(index, buffer.ToArray());
        }


        public void SendLobbyCreated(int index)
        {
            ByteBuffer buffer = new ByteBuffer();
            buffer.WriteInteger((int)Enumerations.ServerPackets.SLobbyCreated);
            buffer.WriteInteger(Globals.Player[index].lobbyIndex);
            buffer.WriteInteger(index);
            buffer.WriteString(Globals.Player[index].Nickname);

            SendDataTo(index, buffer.ToArray());
        }

        public void SendLobbyFound(int index)
        {
            int countOfPlayers = Globals.Lobbies[Globals.Player[index].lobbyIndex].Players.Count;
            ByteBuffer buffer = new ByteBuffer();
            buffer.WriteInteger((int)Enumerations.ServerPackets.SLobbyFound);
            buffer.WriteString(Globals.Player[index].Nickname);
            buffer.WriteInteger(Globals.Player[index].lobbyIndex);
            buffer.WriteInteger(countOfPlayers);

            for (int i = 0; i < countOfPlayers; i++)
            {
                buffer.WriteInteger(Globals.Lobbies[Globals.Player[index].lobbyIndex].Players[i].index);
                buffer.WriteString(Globals.Lobbies[Globals.Player[index].lobbyIndex].Players[i].Nickname);
            }

            SendDataTo(index, buffer.ToArray());
        }

        public void SendLobbyAddedPlayer(int lobbyIndex, int AddedPlayerIndex)
        {
            int countOfPlayers = Globals.Lobbies[Globals.Player[lobbyIndex].lobbyIndex].Players.Count;
            ByteBuffer buffer = new ByteBuffer();
            buffer.WriteInteger((int)Enumerations.ServerPackets.SLobbyAddPlayer);
            buffer.WriteInteger(AddedPlayerIndex);
            buffer.WriteString(Globals.Player[AddedPlayerIndex].Nickname);


            SendDataToAllInLobbyBut(lobbyIndex, AddedPlayerIndex, buffer.ToArray());
        }


        public void SendLobbyStarted(int lobbyIndex, int diff)
        {
            int countOfPlayers = Globals.Lobbies[Globals.Player[lobbyIndex].lobbyIndex].Players.Count;

            List<List<Character>> map = Globals.gameLogic.GetCharMap(diff);

            Globals.Lobbies[lobbyIndex].Diffiult = diff;
            Globals.Lobbies[lobbyIndex].CharMap = map;

            ByteBuffer buffer = new ByteBuffer();
            buffer.WriteInteger((int)Enumerations.ServerPackets.SLobbyStart);
            buffer.WriteInteger(countOfPlayers);
            for (int i = 0; i < countOfPlayers; i++)
            {
                buffer.WriteString(Globals.Lobbies[lobbyIndex].Players[i].Nickname);
                buffer.WriteInteger(i+1);
            }
            buffer.WriteInteger(diff);

            for (int y = 0; y < diff; y++)
            {
                for (int x = 0; x < diff; x++)
                {
                    buffer.WriteInteger(map[x][y]._FirstName);
                    buffer.WriteInteger(map[x][y]._SecondName);
                    buffer.WriteInteger(map[x][y]._isMale);
                    buffer.WriteInteger(map[x][y]._power);
                    buffer.WriteInteger(map[x][y]._wisdom);
                    buffer.WriteInteger(map[x][y]._loyalty);
                    buffer.WriteInteger(map[x][y]._sprite);
                    buffer.WriteInteger(map[x][y]._INTPoliceLoyal);
                    buffer.WriteInteger(map[x][y]._INTMafiaLoyal);
                    buffer.WriteInteger(map[x][y]._INTAssasinLoyal);
                    buffer.WriteInteger(map[x][y]._INTCultLoyal);
                    buffer.WriteInteger(map[x][y]._roleID);
                    buffer.WriteInteger(map[x][y]._itemID);

                    //FirstName, int SecondName, bool isMale, int power, int wisdom, int loyalty, int sprite, int INTPoliceLoyal, int INTMafiaLoyal, int INTAssasinLoyal, int INTCultLoyal, int roleID, int itemID
                }
            }

            SendDataToAllInLobby(lobbyIndex, buffer.ToArray());
        }






        public void SendGamePlayerStartTurn(int PlayerIndex)
        {
            ByteBuffer buffer = new ByteBuffer();
            buffer.WriteInteger((int)Enumerations.ServerPackets.SGameStartTurn);
            
            SendDataTo(PlayerIndex, buffer.ToArray());
        }



        public void SendGamePlayerMoveBlock(int PlayerIndex, int _X, int _Y, int Dir, int Distance)
        {
            ByteBuffer buffer = new ByteBuffer();
            buffer.WriteInteger((int)Enumerations.ServerPackets.SGameMoveBlock);
            buffer.WriteInteger(_X);
            buffer.WriteInteger(_Y);
            buffer.WriteInteger(Dir);
            buffer.WriteInteger(Distance);


            SendDataToAllInLobbyBut(Globals.Lobbies[Globals.Player[PlayerIndex].lobbyIndex].Index, PlayerIndex, buffer.ToArray());
        }




        public void SendGamePlayerEndTurn(int PlayerIndex)
        {
            ByteBuffer buffer = new ByteBuffer();
            buffer.WriteInteger((int)Enumerations.ServerPackets.SGameEndTurn);

            
            SendDataTo(PlayerIndex, buffer.ToArray());
        }


        //public async void SendStartInWorld(int index)
        //{
        //    ByteBuffer buffer = new ByteBuffer();
        //    buffer.WriteInteger((int)Enumerations.ServerPackets.SLogin);
        //    SendDataTo(index, buffer.ToArray());

        //    //Swnd Player data to everyone incl himself
        //    SendDataTo(index, PlayerData(index));
        //    //Send all players to the player himself
        //    for (int i = 1; i < Constants.MAX_PLAYERS; i++)
        //    {
        //        if (Globals.general.IsPlaying(i))
        //        {
        //            if (i != index)
        //            {
        //                await Task.Delay(75);
        //                SendDataTo(index, PlayerData(i));
        //            }
        //        }
        //    }

        //    Console.WriteLine("Player: " + Globals.Player[index].Nickname + " start game.");

        //}

        public async void makeLobby()
        {
            Player player1 = null;
            Player player2 = null;
            for (int i = 1; i < Constants.MAX_PLAYERS; i++)
            {
                if (Globals.general.IsPlaying(i))
                {
                    if (Globals.Player[i].inSearch == true)
                    {
                        player1 = Globals.Player[i];
                        break;
                    }
                }
            }
            if (player1 == null)
                return;

            for (int i = 1; i < Constants.MAX_PLAYERS; i++)
            {
                if (Globals.general.IsPlaying(i))
                {
                    if (Globals.Player[i].inSearch == true)
                    {
                        if (Globals.Player[i] != player1)
                        {
                            player2 = Globals.Player[i];
                            break;
                        }
                    }
                }
            }
            if (player2 == null)
                return;

            Globals.Games.newGame(player1, player2);
            //SendIngame(player1.index, player2.index);
            //SendIngame(player2.index, player2.index);
            Console.WriteLine("New battle created beetween " + player1.Nickname + " and " + player2.Nickname + " .");
        }

        //public void SendPlayerDecks(int index, Deck deck, Deck GDeckDeck, Deck GDeckHand, Deck GDeckBF, Deck GDeckGrave, Deck GDeckStack, Deck GEnemyDeckStack, Deck GEnemyDeckBattlefield, int count, float time, int hp, int inf)
        //{

        //    ByteBuffer buffer = new ByteBuffer();

        //    buffer.WriteInteger((int)Enumerations.ServerPackets.SDeckChanges);

        //    //Player info
        //    buffer.WriteInteger(index);
        //    buffer.WriteBytes(buffer.SetDeck(Globals.general.GetPlayerDeck(index)));
        //    buffer.WriteBytes(buffer.SetDeck(Globals.general.GetPlayerGDeckDeck(index)));
        //    buffer.WriteBytes(buffer.SetDeck(Globals.general.GetPlayerGDeckHand(index)));
        //    buffer.WriteBytes(buffer.SetDeck(Globals.general.GetPlayerGDeckBF(index)));
        //    buffer.WriteBytes(buffer.SetDeck(Globals.general.GetPlayerGDeckGrave(index)));
        //    buffer.WriteBytes(buffer.SetDeck(Globals.general.GetPlayerGDeckStack(index)));
        //    buffer.WriteBytes(buffer.SetDeck(Globals.general.GetEnemyPlayerGDeckStack(index)));
        //    buffer.WriteBytes(buffer.SetDeck(Globals.general.GetEnemyGDeckBF(index)));
        //    buffer.WriteInteger(Globals.general.GetEnemyHandCount(index));
        //    buffer.WriteFloat(Globals.general.GetPlayerTime(index));
        //    buffer.WriteInteger(Globals.general.GetPlayerHp(index));
        //    buffer.WriteInteger(Globals.general.GetPlayerInf(index));
        //    //

        //    SendDataToAllBut(index, buffer.ToArray());
        //}
    }
}
