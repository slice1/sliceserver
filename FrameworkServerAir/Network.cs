﻿using System;
using System.Net.Sockets;
using System.Net;

namespace FrameworkServerAir
{
    class Network
    {
        public TcpListener ServerSocket;


        public void InitTCP()
        {
            ServerSocket = new TcpListener(IPAddress.Any, 42777);
            ServerSocket.Start();
            ServerSocket.BeginAcceptTcpClient(OnClientConnect, null);

            Console.WriteLine("Server successfully started.");
        }

        void OnClientConnect(IAsyncResult result)
        {
            TcpClient client = ServerSocket.EndAcceptTcpClient(result);
            client.NoDelay = false;
            ServerSocket.BeginAcceptTcpClient(OnClientConnect, null);


            for (int i = 1; i < 100; i++)
            {
                if (Globals.Clients[i].Socket == null)
                {
                    Globals.Clients[i].Socket = client;
                    Globals.Clients[i].Index = i;
                    Globals.Clients[i].IP = client.Client.RemoteEndPoint.ToString();
                    Globals.Clients[i].Start();
                    Console.WriteLine("Incoming connection from " + Globals.Clients[i].IP + " || Index: " + Globals.Clients[i].Index);

                    return;
                }
            }
        }
    }
}
