﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrameworkServerAir
{
    class General
    {
        public void InitServer()
        {
            InitGameData();
            Globals.network.InitTCP();
        }

        public void InitGameData()
        {
            for (int i = 1; i < Constants.MAX_PLAYERS; i++)
            {
                Globals.Clients[i] = new Client();
                Globals.tempPlayers[i] = new TempPlayer();
                Globals.Player[i] = new Player();
            }
        }

        public bool IsPlaying(int index)
        {
            if (Globals.Clients[index] != null)
            {
                if (Globals.tempPlayers[index].inLobby)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        //public Deck GetPlayerDeck(int index)
        //{
        //    //prevent from 

        //    if (index <= 0 | index > Constants.MAX_PLAYERS)
        //    {
        //        return null;
        //    }
        //    return Globals.Player[index].deck;
        //}

        public int GetPlayerInf(int index)
        {
            if (index <= 0 | index > Constants.MAX_PLAYERS)
            {
                return 0;
            }
            return Globals.Player[index]._inf;
        }
        public int GetPlayerHp(int index)
        {
            if (index <= 0 | index > Constants.MAX_PLAYERS)
            {
                return 0;
            }
            return Globals.Player[index]._hp;
        }

        public float GetPlayerTime(int index)
        {
            if (index <= 0 | index > Constants.MAX_PLAYERS)
            {
                return 0;
            }
            return Globals.Player[index]._time;
        }

        public void SetPlayerTime(int index, float N)
        {
            if (index <= 0 | index > Constants.MAX_PLAYERS)
            {
                return;
            }
            Globals.Player[index]._time = N;
        }
        public void SetPlayerHP(int index, int N)
        {
            if (index <= 0 | index > Constants.MAX_PLAYERS)
            {
                return;
            }
            Globals.Player[index]._hp = N;
        }
        public void SetPlayerInf(int index, int N)
        {
            if (index <= 0 | index > Constants.MAX_PLAYERS)
            {
                return;
            }
            Globals.Player[index]._inf = N;
        }
    }
}
