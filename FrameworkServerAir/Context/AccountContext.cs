﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.Entity;
using System.Linq;

namespace FrameworkServerAir
{
    public class AccountContext : DbContext
    {
        public AccountContext()
            : base("DbConnection")
        { }

        public DbSet<Account> Accounts { get; set; }

        public void addAcc(string username, string password)
        {
            Account acc = new Account { Name = username, Password = password };
            Accounts.Add(acc);
            SaveChanges();
            Console.WriteLine("New account created. Username: " + username + " || Password: " + password);
        }

        public Account GetUser(Account logUser)
        {
            
            var users = Accounts.Where(u => u.Name == logUser.Name && u.Password == logUser.Password);
            Account usr = new Account();
            foreach (Account u in users)
            {
                usr = u;
            }
            return usr;
            
        }

        public bool AccountExist(int index, string username)
        {
            Console.WriteLine("acc exist?");
            foreach (Account acc in Accounts)
            {
                if (acc.Name == username)
                {
                    Console.WriteLine("acc exist");
                    return true;
                }
            }
            Console.WriteLine("acc NOT exist");
            Globals.networkSendData.SendAlertMsg(index, "Account does not exist");
            return false;
        }


        public bool PasswordOK(int index, string username, string password)
        {
            Console.WriteLine("acc pass?");
            foreach (Account acc in Accounts)
            {
                if (acc.Name == username && acc.Password == password)
                {
                    Console.WriteLine("acc pass");
                    return true;
                }
            }
            Console.WriteLine("acc NOT pass");
            Globals.networkSendData.SendAlertMsg(index, "Password or login does not match");
            return false;
        }

        public void LoadPlayer(int index, string username)
        {
            Console.WriteLine("Account loading. Username: " + username);
            Account acc = new Account {Name = username};

            foreach (Account ac in Accounts)
            {
                if (ac.Name == username)
                {
                    acc = ac;
                }
            }

            Globals.Player[index].Nickname = acc.Name;
            Globals.Player[index].Gold = acc._gold;
            Globals.Player[index].index = index;
            Console.WriteLine("Account loading done succesfully. Username: " + username);
        }

        //public Deck retDeck()
        //{
        //    Account acc = new Account { Name = "aaa" };
        //    return acc._deck;
        //}

        //public void SavePlayer(int index)
        //{
        //    Console.WriteLine("Account saving. Username: " + Globals.Player[index].Nickname);
        //    Account acc = new Account { Name = Globals.Player[index].Nickname };

        //    foreach (Account ac in Accounts)
        //    {
        //        if (ac.Name == acc.Name)
        //        {
        //            acc = ac;
        //        }
        //    }
            
        //    acc._deck = Globals.Player[index].deck;
        //    acc._game = Globals.Player[index].game;



        //    SaveChanges();
        //    Console.WriteLine("Account saving done succesfully. Username: " + Globals.Player[index].Nickname);

        //}
    }
}
