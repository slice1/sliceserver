﻿using System;
using System.Collections.Generic;
using System.Data.Entity;

namespace FrameworkServerAir
{
    class GameContext : DbContext
    {
        public GameContext()
           : base("GameContext")
        { }

        public int ID { get; set; }
        public DbSet<Game> Games { get; set; }
        public DbSet<Player> players { get; set; }



        public void newGame(Player player1, Player player2)
        {
            Game g = new Game { Id = Globals.GetRandom.Next(), IsPlaying = true };

            player1.enemy = player2;
            player1.game = g;
            player2.enemy = player1;
            player2.game = g;

            players.AddRange(new List<Player> { player1, player2 });
            SaveChanges();
        }

        public void setWinner(Player player)
        {
            player.enemy.Winner = false;
            player.Winner = true;
            player.game.IsPlaying = false;
        }
    }
}
