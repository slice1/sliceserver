﻿namespace FrameworkServerAir
{
    internal class Constants
    {
        public const int MAX_PLAYERS = 100;
        public const int MAX_LOBBIES = 100;

        public const string PATH_DATA = "data/";
        public const string PATH_ACCOUNT = "accounts/";
    }
}