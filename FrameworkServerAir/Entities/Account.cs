﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace FrameworkServerAir
{
    [Table("AccountsTables")]
    public class Account
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
        public Game _game { get; set; }
        public int _gold { get; set; }

        public Player _player { get; set; }
    }
}
