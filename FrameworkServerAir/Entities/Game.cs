﻿using System;
using System.Collections.Generic;

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace FrameworkServerAir
{
    public class Game
    {
        public int Id { get; set; }
        public bool IsPlaying { get; set; }

        public ICollection<Player> Players { get; set; }
        public Game()
        {
            Players = new List<Player>();
        }



    }
}
