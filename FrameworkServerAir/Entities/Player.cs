﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FrameworkServerAir
{
    public class Player 
    {
        public int _inf { get; set; }
        public int _hp { get; set; }
        public float _time { get; set; }

        //General
        public string Nickname;
        public int index;
        public int lobbyIndex;
        public bool Winner { get; set; }


        public bool inSearch = false;
        public bool inPlay = false;
        public int Gold { get; set; }

        //Position
        [Key]
        [ForeignKey("account")]
        public int Id { get; set; }
        public Account account { get; set; }
        public Game game { get; set; }
        public Player enemy { get; set; }
        public int CardsInHand { get; set; }
        //

        public bool isMoved, isActed;
    }
}
